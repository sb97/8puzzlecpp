#include "algorithm.h"
#include <stdlib.h>
#include <stack>
#include <queue>
#include <list>
#include <array>
#include <string.h>
#include <functional>
#include <algorithm>

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////
//
// Search Algorithm:  Breadth-First Search   
//
// Author: Kirsten
//
////////////////////////////////////////////////////////////////////////////////////////////

string breadthFirstSearch(string const initialState, string const goalState, int &numOfStateExpansions, int& maxQLength, float &actualRunningTime){
	string path;
	clock_t startTime;
	//add necessary variables here
	char parent;
	int count = 0, inversionCount = 0, i = 0, j = 0;
	Puzzle *puzzle = new Puzzle(initialState, goalState);
	queue <Puzzle*> q;
	startTime = clock();
	numOfStateExpansions = 0;
	maxQLength = 0;

	//algorithm implementation
	cout << "------------------------------" << endl;
	cout << "<<breadthFirstSearch>>" << endl;
	cout << "------------------------------" << endl;

	// Checks if the puzzle is solvable (algorithm from GeeksForGeeks.org)
	for(i = 0; i < 8; i++){
		for(j = i+1; j < 9; j++){
			if(initialState[i] && initialState[j] && initialState[i] > initialState[j]){
				inversionCount++;
			}
		}
	}
	if (inversionCount % 2 != 0){
		cout << "Puzzle is not solvable" << endl;
		path = "";
	} else{
		while(true){ // Loop
			if(puzzle->goalMatch()){ // Check if board is in the goal state
				break;
			}
			numOfStateExpansions++; // Track number of tried options
			parent = puzzle->getPath()[puzzle->getPathLength()-1]; // Get previous move

			if(puzzle->canMoveUp() && parent != 'D'){ // Check if blank space can move and if it was just there
				q.push(puzzle->moveUp()); // Add moved board to queue
				count++; // Add 1 to queue length
				if(count > maxQLength){ // Track max queue length
					maxQLength = count;
				}
			}
			if(puzzle->canMoveRight() && parent != 'L'){
				q.push(puzzle->moveRight());
				count++;
				if(count > maxQLength){
					maxQLength = count;
				}
			}
			if(puzzle->canMoveDown() && parent != 'U'){
				q.push(puzzle->moveDown());
				count++;
				if(count > maxQLength){
					maxQLength = count;
				}
			}
			if(puzzle->canMoveLeft() && parent != 'R'){
				q.push(puzzle->moveLeft());
				count++;
				if(count > maxQLength){
					maxQLength = count;
				}
			}
			delete puzzle; // Delete current board
			puzzle = q.front(); // Create new board from q
			q.pop(); // Remove board from q
			count--; // Minus 1 from queue length
		}
		if(puzzle->goalMatch()){ // If solution found set path, else no path
			path = puzzle->getPath();
		}else{
			path = "";
		}
	}

//***********************************************************************************************************
	actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC); // Get time
	return path; // Return path to goal state
}

///////////////////////////////////////////////////////////////////////////////////////////
//
// Search Algorithm:  Breadth-First Search with VisitedList
//
// Author: Kirsten  
//
////////////////////////////////////////////////////////////////////////////////////////////

// Visited list
bool visitedPuzzle(vector<size_t>& hashes, Puzzle *puzzle) {
	// Calculate hash
	hash<string> hashfnc;
	size_t hash = hashfnc(puzzle->getString());
	// Check all hash isn't in hash list already
	if (find(hashes.begin(), hashes.end(), hash) != hashes.end()) return true;	// Puzzle has been visited already
	hashes.push_back(hash);
	return false;
}

string breadthFirstSearch_with_VisitedList(string const initialState, string const goalState, int &numOfStateExpansions, int& maxQLength, float &actualRunningTime){
	string path;
	clock_t startTime;
	//add necessary variables here
	char parent;
	int count = 0, inversionCount = 0, i = 0, j = 0;
	Puzzle *puzzle = new Puzzle(initialState, goalState);
	queue <Puzzle*> q;
	startTime = clock();
	numOfStateExpansions = 0;
	maxQLength = 0;
	vector<size_t> hashes;

	//algorithm implementation
	cout << "------------------------------" << endl;
	cout << "<<breadthFirstSearch_with_VisitedList>>" << endl;
	cout << "------------------------------" << endl;
	
	// Checks if the puzzle is solvable (algorithm from GeeksForGeeks.org)
	for(i = 0; i < 8; i++){
		for(j = i+1; j < 9; j++){
			if(initialState[i] && initialState[j] && initialState[i] > initialState[j]){
				inversionCount++;
			}
		}
	}
	if (inversionCount % 2 != 0){
		cout << "Puzzle is not solvable" << endl;
		path = "";
	} else{
		while(true){ // Loop
			if(puzzle->goalMatch()){ // Check if board is in the goal state
				break;
			}
			numOfStateExpansions++; // Track number of tried options
			parent = puzzle->getPath()[puzzle->getPathLength()-1]; // Get previous move

			if(puzzle->canMoveUp() && parent != 'D'){ // Check if blank space can move and if it was just there
				if(!visitedPuzzle(hashes, puzzle->moveUp())) {
					q.push(puzzle->moveUp()); // Add moved board to queue
					count++; // Add 1 to queue length
					if(count > maxQLength){ // Track max queue length
						maxQLength = count;
					}
				}
			}
			if(puzzle->canMoveRight() && parent != 'L'){
				if(!visitedPuzzle(hashes, puzzle->moveRight())) {
					q.push(puzzle->moveRight());
					count++;
					if(count > maxQLength){
						maxQLength = count;
					}
				}
			}
			if(puzzle->canMoveDown() && parent != 'U'){
				if(!visitedPuzzle(hashes, puzzle->moveDown())) {
					q.push(puzzle->moveDown());
					count++;
					if(count > maxQLength){
						maxQLength = count;
					}
				}
			}
			if(puzzle->canMoveLeft() && parent != 'R'){
				if(!visitedPuzzle(hashes, puzzle->moveLeft())) {
					q.push(puzzle->moveLeft());
					count++;
					if(count > maxQLength){
						maxQLength = count;
					}
				}
			}
			delete puzzle; // Delete current board
			puzzle = q.front(); // Create new board from q
			q.pop(); // Remove board from q
			count--; // Minus 1 from queue length
		}
		
		if(puzzle->goalMatch()){ // If solution found set path, else no path
			path = puzzle->getPath();
		}else{
			path = "";
		}
	}

//***********************************************************************************************************
	actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC); // Get time
	return path; // Return path to goal state
}

///////////////////////////////////////////////////////////////////////////////////////////
//
// Search Algorithm: Progressive Deepening Search without Visited List  
//
// Author: Stefan  
//
//////////////////////////////////////////////////////////////////////////////////////////

/*string depthLimitedSearch(string const initial, string const goal, int depth){
	Puzzle * childL;
	Puzzle * childR;
	Puzzle * childU;
	Puzzle * childD;
	Puzzle * children[4];
	string found;
	Puzzle * puzz = new Puzzle(initial, goal);
	stack <Puzzle*> p;
	p.push(puzz);


	int num = 0;
	if(depth == 0 && p.top() -> goalMatch()) return p.top() -> getPath();
	if(depth > 0){
		if(p.top() -> canMoveUp()){
			childU = p.top() -> moveUp();
			children[num] = childU;
			num++;
		} 
		if(p.top() -> canMoveRight()){
			childR = p.top() -> moveRight();
			children[num] = childR;
			num++;
		}
		if(p.top() -> canMoveDown()){
			childD = p.top() -> moveDown();
			children[num] = childD;
			num++;
		} 
		if(p.top() -> canMoveLeft()){
			childL = p.top() -> moveLeft();
			children[num] = childL;
			num++;
		}
		for(int i = 0; i < num; i++){
			if(children[i] -> goalMatch()) found = depthLimitedSearch(children[i] -> toString(), goal, depth -1);
			if(found != "") return found;
		}
	return NULL;
	}
}*/

string progressiveDeepeningSearch_No_VisitedList(string const initialState, string const goalState, int &numOfStateExpansions, int& maxQLength, float &actualRunningTime, int ultimateMaxDepth){}
/*string path;
	clock_t startTime;
	int depth = 1;
	Puzzle * puzz = new Puzzle(initialState, goalState);
	string found;
    //add necessary variables here

    //algorithm implementation
	cout << "------------------------------" << endl;
	cout << "<<progressiveDeepeningSearch_No_VisitedList>>" << endl;
 	cout << "------------------------------" << endl;

	startTime = clock();
	//srand(time(NULL)); //RANDOM NUMBER GENERATOR - ONLY FOR THIS DEMO.  YOU REALLY DON'T NEED THIS! DISABLE THIS STATEMENT.
	//maxQLength= rand() % 500; //AT THE MOMENT, THIS IS JUST GENERATING SOME DUMMY VALUE.  YOUR ALGORITHM IMPLEMENTATION SHOULD COMPUTE THIS PROPERLY.
	//numOfStateExpansions = rand() % 600; //AT THE MOMENT, THIS IS JUST GENERATING SOME DUMMY VALUE.  YOUR ALGORITHM IMPLEMENTATION SHOULD COMPUTE THIS PROPERLY
	while(true){
		found = depthLimitedSearch(initialState, goalState, depth);
		depth++;
		if(found != ""){
			return found;
		}
	}
	
	
//***********************************************************************************************************
	actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC);
	//path = "DDRRLLLUUU";  //this is just a dummy path for testing the function           
	return found;		
}*/



///////////////////////////////////////////////////////////////////////////////////////////
//
// Search Algorithm: Progressive Deepening Search with Non-Strict Visited List
//
// Author: Stefan  
//
////////////////////////////////////////////////////////////////////////////////////////////

string progressiveDeepeningSearch_with_NonStrict_VisitedList(string const initialState, string const goalState, int &numOfStateExpansions, int& maxQLength, float &actualRunningTime, int ultimateMaxDepth){}
   /* string path;
	clock_t startTime;
    //add necessary variables here


    //algorithm implementation
	// cout << "------------------------------" << endl;
 //    cout << "<<progressiveDeepeningSearch_with_NonStrict_VisitedList>>" << endl;
 //    cout << "------------------------------" << endl;

	startTime = clock();
	srand(time(NULL)); //RANDOM NUMBER GENERATOR - ONLY FOR THIS DEMO.  YOU REALLY DON'T NEED THIS! DISABLE THIS STATEMENT.
	maxQLength= rand() % 300; //AT THE MOMENT, THIS IS JUST GENERATING SOME DUMMY VALUE.  YOUR ALGORITHM IMPLEMENTATION SHOULD COMPUTE THIS PROPERLY.
	numOfStateExpansions = rand() % 300; //AT THE MOMENT, THIS IS JUST GENERATING SOME DUMMY VALUE.  YOUR ALGORITHM IMPLEMENTATION SHOULD COMPUTE THIS PROPERLY

	
	
//***********************************************************************************************************
	actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC);
	path = "DDRRLLLUUU"; //this is just a dummy path for testing the function
	
	return path;		
		
}*/

////////////////////////////////////////////////////////////////////////////////////////
//
//Search Algorithm: A* with Expanded List
//
//Author: Mieke
//
///////////////////////////////////////////////////////////////////////////////////////

//A function to check whether the puzzle is already in the queue
bool check(priority_queue<Puzzle*, vector<Puzzle*>, greater<Puzzle*>> q, Puzzle* p){
	priority_queue<Puzzle*, vector<Puzzle*>, greater<Puzzle*>> copy = q; 
	for(int i = 0; i < q.size(); i++){
		if(copy.top()==p){return true;}
		copy.pop();
	}
	return false;
}

string aStar_ExpandedList(string const initialState, string const goalState, int &numOfStateExpansions, int& maxQLength, 
                               float &actualRunningTime, int &numOfDeletionsFromMiddleOfHeap, int &numOfLocalLoopsAvoided, int &numOfAttemptedNodeReExpansions, heuristicFunction heuristic){
											 
   string path;
   clock_t startTime;
   numOfDeletionsFromMiddleOfHeap=0;
   numOfLocalLoopsAvoided=0;
   numOfAttemptedNodeReExpansions=0;

   int num = 0;
   int inversionCount = 0;

    /*cout << "------------------------------" << endl;
    cout << "<<aStar_ExpandedList>>" << endl;
    cout << "------------------------------" << endl;*/
	actualRunningTime=0.0;	
	startTime = clock();
	//srand(time(NULL)); //RANDOM NUMBER GENERATOR - ONLY FOR THIS DEMO.  YOU REALLY DON'T NEED THIS! DISABLE THIS STATEMENT.
	maxQLength= 0;
	numOfStateExpansions = 0; //AT THE MOMENT, THIS IS JUST GENERATING SOME DUMMY VALUE.  YOUR ALGORITHM IMPLEMENTATION SHOULD COMPUTE THIS PROPERLY

	Puzzle* p = new Puzzle(initialState, goalState);

	priority_queue<Puzzle*, vector<Puzzle*>, greater<Puzzle*>> open;
	priority_queue<Puzzle*, vector<Puzzle*>, greater<Puzzle*>> closed;
	array<Puzzle*, 9> children;

	// Checks if the puzzle is solvable
	for(int i = 0; i < 8; i++){
		for(int j = i+1; j < 9; j++){
			if(initialState[i] && initialState[j] && initialState[i] > initialState[j]){
				inversionCount++;
			}
		}
	}

	if (inversionCount % 2 != 0){
		cout << "Puzzle is not solvable" << endl;
		path = "";
	} else {  

		open.push(p);

		while(!open.empty()){
			if(open.size() > maxQLength){maxQLength=open.size();}
			Puzzle* current = open.top(); 
			open.pop();
			//if we are at a goal, yay and break
			if(current->goalMatch()){
				actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC);
				path = current->getPath();
				return path;
			}
			//generate successors
			if(current->canMoveUp()){numOfStateExpansions++; Puzzle * CU = current->moveUp(); children[num] = CU; num++;}
			if(current->canMoveRight()){numOfStateExpansions++; Puzzle * CR = current->moveRight(); children[num] = CR; num++;}
			if(current->canMoveDown()){numOfStateExpansions++; Puzzle * CD = current->moveDown(); children[num] = CD; num++;}
			if(current->canMoveLeft()){numOfStateExpansions++; Puzzle * CL = current->moveLeft(); children[num] = CL; num++;}
			int index = num; 
			//for each of the successors
			for(int i = 0; i < index; i++){
				//store this one as current
				Puzzle* p = children[i];
				children[i] = 0; 
				num--;
				if(p->goalMatch()){
					actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC); 
					path = p->getPath(); 
					return path;
				}
				//if the puzzle is in the closed list, ignore it
				if(check(closed, p)){continue;}
				//otherwise, add it to the open list
				open.push(p);
				}
			//add the expanded puzzle to the closed queue
			closed.push(current);
		}
	}
//***********************************************************************************************************
	actualRunningTime = ((float)(clock() - startTime)/CLOCKS_PER_SEC);
	path = "";
	return path;
}
